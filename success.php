<?php
//***************** Страница с завершением заказа ******************
session_start();
 
// формируем массив с товарами в заказе (если товар один - оставляйте только первый элемент массива)
$products_list = array(
    0 => array(
            'product_id' => $_REQUEST['product_id'],    //код товара (из каталога CRM)
            'price'      => $_REQUEST['product_price'], //цена товара 1
            'count'      => '1',                     //количество товара 1
            // если есть смежные товары, тогда количество общего товара игнорируется
            'subs'       => array(
                                0  => array(
                                        'sub_id' => $_REQUEST['product_id'],
                                        'count'  => '1'
                                        ),
                                1  => array(
                                        'sub_id' => $_REQUEST['product_id'],
                                        'count'  => '1'
                                        )
            )
    ),
    1 => array(
            'product_id' => $_REQUEST['product_id'],    //код товара 2 (из каталога CRM)
            'price'      => $_REQUEST['product_price'], //цена товара 2
            'count'      => '1',                     //количество товара 2
            // если есть смежные товары, тогда количество общего товара игнорируется
            'subs'       => array(
                                0  => array(
                                        'sub_id' => $_REQUEST['product_id'],
                                        'count'  => '1'
                                        ),
                                1  => array(
                                        'sub_id' => $_REQUEST['product_id'],
                                        'count'  => '1'
                                        )
            )
        )
);
$products = urlencode(serialize($products_list));
$sender = urlencode(serialize($_SERVER));
// параметры запроса
$data = array(
    'key'             => '5966fb9b831f87006733985e635b6be1', //Ваш секретный токен
    'order_id'        => number_format(round(microtime(true)*10),0,'.',''), //идентификатор (код) заказа (*автоматически*)
    'country'         => 'UA',                         // Географическое направление заказа
    'office'          => '1',                          // Офис (id в CRM)
    'products'        => $products,                    // массив с товарами в заказе
    'bayer_name'      => $_REQUEST['name'],            // покупатель (Ф.И.О)
    'phone'           => $_REQUEST['phone'],           // телефон
    'email'           => $_REQUEST['email'],           // электронка
    'comment'         => $_REQUEST['product_name'],    // комментарий
    'delivery'        => $_REQUEST['delivery'],        // способ доставки (id в CRM)
    'delivery_adress' => $_REQUEST['delivery_adress'], // адрес доставки
    'payment'         => '',                           // вариант оплаты (id в CRM)
    'sender'          => $sender,                        
    'utm_source'      => $_SESSION['utms']['utm_source'],  // utm_source
    'utm_medium'      => $_SESSION['utms']['utm_medium'],  // utm_medium
    'utm_term'        => $_SESSION['utms']['utm_term'],    // utm_term
    'utm_content'     => $_SESSION['utms']['utm_content'], // utm_content
    'utm_campaign'    => $_SESSION['utms']['utm_campaign'],// utm_campaign
    'additional_1'    => '',                               // Дополнительное поле 1
    'additional_2'    => '',                               // Дополнительное поле 2
    'additional_3'    => '',                               // Дополнительное поле 3
    'additional_4'    => ''                                // Дополнительное поле 4
);
 
// запрос
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'http://banda.lp-crm.biz/api/addNewOrder.html');
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
$out = curl_exec($curl);
curl_close($curl);
//$out – ответ сервера в формате JSON

$token = "1477028053:AAGTp_j3FDMtwv06_NGMyw7yJGARYA2wWW0";
$chat_id = "-473084469";
//Определяем переменные для передачи данных из нашей формы

    $name = ($_POST['name']);
    $email = ($_POST['email']);
    $pixel = ($_POST['pixel']);

//Собираем в массив то, что будет передаваться боту
    $arr = array(
        'Имя:' => $name,
        'Почта:' => $email,
        'Пиксель' => $pixel
    );

//Настраиваем внешний вид сообщения в телеграме
    foreach($arr as $key => $value) {
        $txt .= "<b>".$key."</b> ".$value."%0A";
    };

//Передаем данные боту
    $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");

?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width" name="viewport">
        <title>Поздравляем! Ваша заявка принята!</title>
        <style type="text/css">
            body {
                line-height: 1;
                height: 100%;
                font-family: Arial;
                font-size: 15px;
                color: #313e47;
                width: 100%;

                height: 100%;
                padding: 0;
                margin: 0;
                background: url('12354.jpg');}
            h2 {
                margin: 0;
                padding: 0;
                font-size: 32px;
                line-height: 44px;
                color: #313e47;
                text-align: center;
                font-weight: bold;
                text-transform: uppercase;
            }
            a {
                color: #69B9FF; 
            }
            .list_info li span {
                width: auto;
                padding-left: 15px;
                display: inline-block;
                font-weight: bold;
                font-style: normal;
            } 
            .list_info {
               text-align: left;
               display: inline-block;
               list-style: none;
               margin-top: -10px;
               margin-bottom: -11px;
               font-size: 16px;
               margin: 5px auto;
            }
            .list_info li {
                margin: 11px 0px;
            }
            .fail {
                margin: 10px 0 20px 0px;
                text-align: center;
            }
            .email {
                position: relative;
                text-align: center;
                margin-top: 40px;
            }
            .email input {
                height: 30px;
                width: 200px;
                font-size: 14px;
                padding-right: 10px;
                padding-left: 10px;
                outline: none;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                border: 1px solid #B6B6B6; 
                margin-bottom: 10px;
            }
            .block_success {
                max-width: 960px;
                padding: 70px 10px 70px 10px;
                margin: -50px auto;
		background-color: rgba(0,0,0,0.3);
            }
            .success {
                text-align: center;
            }
            .background {
                  position: absolute;
                  top: 0;
                  bottom: 0;
                  left: 0;
                  right: 0;
                  z-index: -1;
            }
h1,h2,h3,p,a{color:#fff}
        </style>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', <?=$_GET['sub'];?>);
  
  fbq('track', 'Lead');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=<<?=$_GET['sub'];?>&ev=Lead&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>

 <body class="background"> 
   
        <div class="block_success">
            <h2>Success!</h2>
            <p class="success">
                Thank you  <br> We sent your email!
            </p>
            
            <div class="success">
                <ul class="list_info">
                    <li><span>Your name:  </span><span id="client"><?=$_POST['name']?></span></li>
                    <li><span>Your phone: </span><span id="tel"><?=$_POST['email']?></span></li>
                </ul>
                <br/><span id="submit"></span>
            </div>
            <p class="fail success">Our manager will sent you email </p>

		<div style="margin: 20px auto;text-align: center">
		<!--<h3 style="font-size: 20px;
    text-transform: uppercase;">Возможность купить ремешок для Вашего брелка!!!
	</h3>
	<p style="font-size: 20px;">По цене: <br> <span style="text-decoration: line-through;">240грн</span><br> 169грн.</p>
	<p>Скажите нашему менеджеру:</br> "<span style="text-transform: uppercase;color:#000;font-weight: 600;font-size: 20px;">Хочу ремешок для БРЕЛКА</span>"</p>
	</div>
	
	<div style="width: 90%;
    margin: 20px auto;
    border: 2px solid grey;
    border-radius: 9px;
    overflow: hidden;
    text-align: center;">
		<a style="text-decoration: none;">
			<img src="remeshok.jpg" alt="" style="width: 100%">
			<p style="color: #fff;
    margin: 0;
    padding: 10px;
    background-color: #74bbfc;text-transform: uppercase;">Ремешок Вашего авто</p>
		</a>
		
	</div>-->
        </div> 
 
 
 </body>
</html>
